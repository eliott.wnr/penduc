#ifndef WORD_H
#define WORD_H

int calculateLenWord(int const difficulty); 
int fileLen(FILE *file); 
void chooseWord(char *wordToGuess, int const lenWord, int const difficulty); 

#endif
